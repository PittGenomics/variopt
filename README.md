## VariOpt

VariOpt enables automating the process of finding optimal tool configurations using [Parsl](https://github.com/Parsl/parsl) and [BayesianOptimization](https://github.com/fmfn/BayesianOptimization).

<!-- See [paropt-service](https://gitlab.com/PittGenomics/variopt/-/tree/master/paropt-service) for using this package in a RESTful API to launch optimization tasks. -->

### Setup

```
bash pip install git+https://gitlab.com:PittGenomics/variopt.git
```

### Usage

Please refer to [Basic Usage](https://gitlab.com/PittGenomics/variopt/-/wikis/Basic-Usage) in [Wiki](https://gitlab.com/PittGenomics/variopt/-/wikis/home).

## Run Using the Published Docker Image

- Pull the published Docker image

```
docker pull jpittlabgenomics/variopt:latest
```

- Run GATK3 example

```
docker run -it \
    -v $PWD/output:/variopt/output \
    jpittlabgenomics/variopt:latest bash -c "python3 /variopt/scripts/gatk3_example.sh"
```
Please note that it may take a few minutes to finish the example. The results are printed to the terminal.

<br/><br/>

## Developer Notes
<br/>

#### Download data files required for the volume mount

```
wget -c -O ~/Downloads/variopt_data.tar.gz https://d19h4fo1krezc1.cloudfront.net/variopt_data.tar.gz
tar -zxf ~/Downloads/variopt_data.tar.gz -C ./
```

#### Build Docker image

```
docker build -t jpittlabgenomics/variopt:latest .
```

#### Run standalone

```
docker run -it \
    -v $PWD/output:/variopt/output \
    -v $PWD/data:/variopt/data \
    jpittlabgenomics/variopt:latest bash -c "/variopt/scripts/gatk3_example.sh"
```

#### Run using VariOpt framework (via Parsl runner)

```
docker run -it \
    -v $PWD/output:/variopt/output \
    -v $PWD/data:/variopt/data \
    jpittlabgenomics/variopt:latest bash -c "python3 /variopt/scripts/gatk3_example.py"
```
