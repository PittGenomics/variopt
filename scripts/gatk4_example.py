import variopt
from variopt.runner import ParslRunner
from variopt.storage import RelationalDB
from variopt.optimizer import BayesianOptimizer, GridSearch, RandomSearch
from variopt.runner.parsl import timeCmd, searchMatrix, variantCallerAccu
from variopt.storage.entities import Parameter, PARAMETER_TYPE_INT, Experiment, LocalCompute, EC2Compute, PBSProCompute

variopt.setConsoleLogger()


gatk4_experiment_cmd_str = f"""
#! /bin/bash
bash ./scripts/gatk4_example.sh min-base-quality-score ${{min_base_quality_score}}
"""

local_compute = LocalCompute(max_threads=4)
experiment_inst = Experiment(
    tool_name=f'GATK4_HaplotypeCaller',
    parameters=[
        Parameter(name="min_base_quality_score",
                  type=PARAMETER_TYPE_INT, minimum=0, maximum=8)
    ],
    command_template_string=gatk4_experiment_cmd_str,
    compute=local_compute
)

storage = RelationalDB(
    'sqlite',
    '',
    '',
    '',
    './output/local.db',
)

optimizer = BayesianOptimizer(
    n_init=2,
    n_iter=1,
    alpha=1e-4,
    kappa=10,
)

obj_func = 'new_sigmoid'
alpha = 1e-2
baseline_time = 50
sensitivity = 2

po = ParslRunner(
    obj_func=getattr(variopt.runner.parsl, "localConstrainedObjective"),
    obj_func_params={
        'f1_boundary': 0.1,
        'objective': obj_func,
        'baseline_time': baseline_time,
        'alpha': alpha,
        'sensitivity': sensitivity
    },
    optimizer=optimizer,
    storage=storage,
    experiment=experiment_inst,
    logs_root_dir='./output')

po.run()
