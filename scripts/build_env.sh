#!/bin/bash
set -e

BASE_DIR=${1-~/variopt}
CONDA_ENV_PY2_BIOTOOLS="py2_biotools"

mkdir -p $BASE_DIR
cd $BASE_DIR

### Bio-tools env ###
# create conda env for bio tools (use Python 2.7 since hap.py is only compatible with py2.7)
if ! echo $ENVS | grep -q "^${CONDA_ENV_PY2_BIOTOOLS}$"; then
  conda create -y --name $CONDA_ENV_PY2_BIOTOOLS python=2.7
fi
if [ -f ~/.bashrc ]; then
  . ~/.bashrc
else
  . ~/.bash_profile
fi
conda activate $CONDA_ENV_PY2_BIOTOOLS

# install base py packages
pip install setuptools psutil numpy pandas distribute pysam scipy bx-python

# install base bio-tools and freebayes, platypus
conda install -y -c bioconda \
  freebayes=1.3.2 \
  bamtools=2.5.1 \
  platypus-variant=0.8.1 \
  samtools=1.3.1 \
  openssl=1.0 \
  bcftools=1.9 \
  tabix=0.2.6 \
  picard=2.5.0

# install gatk4
conda install -y -c bioconda gatk4=4.1.9.0

# install gatk3, download and register GATK3 jar file
conda install -y -c bioconda gatk=3.8
wget -c -O "${BASE_DIR}/GenomeAnalysisTK-3.8-1-0-gf15c1c3ef.tar.bz2" \
        "https://storage.googleapis.com/gatk-software/package-archive/gatk/GenomeAnalysisTK-3.8-1-0-gf15c1c3ef.tar.bz2"
tar xjvf "${BASE_DIR}/GenomeAnalysisTK-3.8-1-0-gf15c1c3ef.tar.bz2" -C $BASE_DIR
gatk-register "${BASE_DIR}/GenomeAnalysisTK-3.8-1-0-gf15c1c3ef/GenomeAnalysisTK.jar"

# download and install boost
wget -c -O "${BASE_DIR}/boost_1_73_0.tar.bz2" https://boostorg.jfrog.io/artifactory/main/release/1.73.0/source/boost_1_73_0.tar.bz2
tar -xf "${BASE_DIR}/boost_1_73_0.tar.bz2" -C $BASE_DIR
pushd $BASE_DIR/boost_1_73_0
./bootstrap.sh --with-libraries=filesystem,chrono,thread,iostreams,system,regex,test,program_options
./b2 --prefix=$BASE_DIR/boost_1_73_0_install install
popd

# download hap.py and build
[ ! -d $BASE_DIR/hap.py ] && git clone --depth 1 --branch v0.3.12 https://github.com/sequencing/hap.py $BASE_DIR/hap.py
mkdir -p $BASE_DIR/hap.py_build
cd $BASE_DIR/hap.py_build
echo "building hap.py from source..."

# fix for 'sys/types.h' file not found in MacOS
if [[ "$OSTYPE" == "darwin"* ]]; then
  export CFLAGS="-I/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include"
fi
cmake ../hap.py \
  -DBOOST_ROOT=$BASE_DIR/boost_1_73_0_install \
  -DBoost_INCLUDE_DIR=$BASE_DIR/boost_1_73_0_install/include \
  -DBoost_LIBRARY_DIR=$BASE_DIR/boost_1_73_0_install/lib \
  -DBUILD_VCFEVAL=ON

make

echo "setup completed"
