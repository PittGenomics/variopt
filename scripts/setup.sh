set -e

# basic
apt update
sudo apt install wget upzim vim

# install miniconda
cd /home
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -b
/root/miniconda3/bin/conda init
source /root/.bashrc

# install hap.py
## basic packages
#sudo apt install -y autoconf build-essential bzip2 cmake cython git libbz2-dev libncurses5-dev openjdk-8-jdk pkg-config software-properties-common zlib1g-dev
#pip install setuptools psutil numpy pandas distribute pysam scipy bx-python

# install strelka
#wget https://github.com/Illumina/strelka/releases/download/v2.9.2/strelka-2.9.2.centos6_x86_64.tar.bz2
#tar xvjf strelka-2.9.2.centos6_x86_64.tar.bz2
# to test installation,
# bash strelka-2.9.2.centos6_x86_64/bin/runStrelkaSomaticWorkflowDemo.bash
# bash strelka-2.9.2.centos6_x86_64/bin/runStrelkaGermlineWorkflowDemo.bash