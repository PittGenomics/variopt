#!/bin/bash

# Some code in this script comes from https://github.com/Illumina/hap.py/blob/master/example/happy/microbenchmark.sh
set -e
. /opt/conda/etc/profile.d/conda.sh
conda activate py2_biotools

VARIOPT_PARAM=${1-min_base_quality_score}
VARIOPT_PARAM_VAL=${2-10}

BASE_DIR=${3-~/variopt}
SCRIPTS_DIR=/variopt/scripts
OUTPUT_DIR=/variopt/output/gatk3_example
DATA_DIR=/variopt/data
chr=21

bam="${DATA_DIR}/bam/test.bam"
ref="${DATA_DIR}/reference/GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna"
true_file="${DATA_DIR}/truth/chr${chr}.vcf.gz"
bed_file="${DATA_DIR}/truth/chr${chr}.bed"

index=${VARIOPT_PARAM}_${VARIOPT_PARAM_VAL}
out_vcf="${OUTPUT_DIR}/${index}.vcf.gz"
out_filtered_vcf="${OUTPUT_DIR}/${index}_filtered.vcf.gz"
out_file="${OUTPUT_DIR}/${index}.txt"

mkdir -p $OUTPUT_DIR
echo "Cleaning previous results..."
rm $out_file 2>/dev/null || true

echo "Running HaplotypeCaller..."
start=$(date +%s%N | cut -b1-13)

java -jar "${CONDA_PREFIX}/bin/../opt/gatk-3.8/GenomeAnalysisTK.jar" \
  -R $ref \
  -T HaplotypeCaller \
  -I $bam \
  -nct 24 \
  -o $out_vcf \
  --${VARIOPT_PARAM} ${VARIOPT_PARAM_VAL} \
  -L chr21

end=$(date +%s%N | cut -b1-13)
runtime=$(((end - start)))
echo "HaplotypeCaller analysis completed"

runtime_scaled=$(bc <<< "scale=4; ${runtime}/60000")
echo "Run completed in ${runtime_scaled} minutes"

export HAPPY="${BASE_DIR}/hap.py_build"
export PATH="${HAPPY}/bin:${PATH}"
export HC="${HAPPY}/bin/hap.py"
export HGREF=$ref

compare_out_series="${OUTPUT_DIR}/${index}_happy_out"
compare_out_file="${OUTPUT_DIR}/${index}_happy_out.summary.csv"

echo "Filtering VCF output..."
bcftools filter -s LowQual -e '%QUAL<30' ${out_vcf} -o ${out_filtered_vcf} -Oz

echo "Running hap.py analysis..."
hap.py ${true_file} ${out_filtered_vcf} \
  -f ${bed_file} \
  -r ${ref} \
  -o ${compare_out_series} \
  --engine=vcfeval \
  --roc QUAL \
  --roc-filter LowQual

echo "Generating report..."
echo "Running time of HaplotypeCaller in 0.001 second: ${runtime}" >>${out_file}
python3 "${SCRIPTS_DIR}/read_csv.py" -i ${compare_out_file} -o ${out_file} -v

# runtime float string value must be printed at the end (required by localConstrainedObjective)
echo "${runtime_scaled}"
