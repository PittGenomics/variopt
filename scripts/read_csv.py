import sys
import getopt

import pandas as pd


def main(argv):
    inputfile = ''
    outputfile = ''
    verbose = False
    try:
        opts, args = getopt.getopt(argv, "hvi:o:", ["input=", "output="])
    except getopt.GetoptError:
        print("read_csv.py -i <inputfile> -o <outputfile>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("read_csv.py -i <inputfile> -o <outputfile>")
            sys.exit()
        elif opt in ("-i", "--input"):
            inputfile = arg
        elif opt in ("-o", "--output"):
            outputfile = arg
        elif opt == "-v":
            verbose = True

    with open(inputfile, 'rb') as f:
        data = pd.read_csv(f)
        print(data)
        prec = data.iloc[3]['METRIC.Precision']
        recall = data.iloc[3]['METRIC.Recall']
        f1 = data.iloc[3]['METRIC.F1_Score']

    if verbose:
        print(f"Precision: {prec}, Recall: {recall}, F1: {f1}")

    with open(outputfile, 'a') as f:
        f.write(f"Precision: {prec}\n")
        f.write(f"Recall: {recall}\n")
        f.write(f"F1: {f1}\n")


if __name__ == "__main__":
    main(sys.argv[1:])
