FROM continuumio/miniconda3:4.9.2

RUN apt-get update --fix-missing && \
    mkdir -p /usr/share/man/man1 && \
    apt-get install -y wget build-essential vim git \
        autoconf bzip2 cmake cython git libbz2-dev libncurses5 libncurses5-dev ant pkg-config software-properties-common zlib1g-dev bc

WORKDIR /variopt

# create a runtime environment for bio-tools
COPY scripts/build_env.sh /variopt/scripts/build_env.sh
RUN /variopt/scripts/build_env.sh

# create a runtime environment for VariOpt framework and set it as the default
RUN conda create -y -n variopt python=3.7 && \
    echo "source activate variopt" > ~/.bashrc
ENV PATH /opt/conda/envs/variopt/bin:$PATH

# make RUN commands use the new environment
SHELL ["conda", "run", "-n", "variopt", "/bin/bash", "-c"]

# copy the entire project and install it as a Python package with dependencies in requirements.txt
COPY . /variopt
RUN chmod -R +x /variopt/scripts && \
    pip3 install /variopt

CMD ["/bin/bash"]